<?php
namespace App\Http\Controllers\Web\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Session;
use Auth;

class AuthController extends Controller
{
     public function get_signup()
    {
        
        return view('website.pages.login');
    }


        public function post_login(Request $request)
    {
        $inputs    = $request->all();
        $rules     = array(
            'mobile_num' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
        $data = $request->all();

        $user =User::where('mobile_num',$data['mobile_num'])->first();
        if ($user) {
            // $otp = rand(1000, 9999);
            $otp = 1111;

            $message = 'Hi, '.$otp
            .' is your one time password(OTP), please enter the same to'
            .' complete your registration process. OTP will expire in 1 hour. Thank you,'
            .' Team Lawn';
            // Send OTP to mobile number
            $res = true;
            // $res = $this->send_otp($message, $data['mobile_num']);

            if ($res) {
                Session::put('otp', $otp);
                Session::put('userid', $user->id);
                    
                return response()->json([
                        'success' => true,
                        'message' => 'Success',
                        'status'=>0,
                        'mobile' => str_pad(substr($data['mobile_num'], -2), strlen($data['mobile_num']), '*', STR_PAD_LEFT)
                    ]
                );
            }
        }
        else{
             // $otp = rand(1000, 9999);
            $otp = 1111;

            $message = 'Hi, '.$otp
            .' is your one time password(OTP), please enter the same to'
            .' complete your registration process. OTP will expire in 1 hour. Thank you,'
            .' Team Lawn';
            // Send OTP to mobile number
            $res = true;
            // $res = $this->send_otp($message, $data['mobile_num']);

            if ($res) {
                Session::put('otp', $otp);
                Session::put('mobile_num',$data['mobile_num']);
                    
                return response()->json([
                        'success' => true,
                        'message' => 'Success',
                        'status'=>1,
                        'mobile' => str_pad(substr($data['mobile_num'], -2), strlen($data['mobile_num']), '*', STR_PAD_LEFT)
                    ]
                );
            }

        }


        return response()->json(array(
            'success' => false,
            'message' => "This mobile number Not Valid "
        ));
    }

    public function submit_signup(Request $request)
    {
        $inputs    = $request->all();
        $rules     = array(
            'full_name' => 'required|max:255',
            'email' => 'required|unique:users,email',
            'mobile_num' => 'required|unique:users,mobile_num'
        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
        $data = $request->all();

        $user = new User;
        $user->full_name = $data['full_name'];
        $user->user_name = '';
        $user->password = '';
        $user->email = $data['email'];
        $user->mobile_num = $data['mobile_num'];
        $user->role_id = 1002;
        $user->otp_verified = 0;
        $res = $user->save();

        if ($res) {
            // $otp = rand(1000, 9999);
            $otp = 1111;

            $message = 'Hi, '.$otp
            .' is your one time password(OTP), please enter the same to'
            .' complete your registration process. OTP will expire in 1 hour. Thank you,'
            .' Team Lawn';
            // Send OTP to mobile number
            $res = true;
            // $res = $this->send_otp($message, $data['mobile_num']);

            if ($res) {
                Session::put('otp', $otp);
                Session::put('userid', $user->id);

                    
                return response()->json([
                        'success' => true,
                        'message' => 'Success',
                        'mobile' => str_pad(substr($data['mobile_num'], -2), strlen($data['mobile_num']), '*', STR_PAD_LEFT)
                    ]
                );
            }
        }

        return response()->json(array(
            'success' => false,
            'message' => "Some error occured"
        ));
    }

    protected function send_otp($message, $mobile_no)
    {
        $sms = new Sms();
        return $sms->send($message, $mobile_no);
    }

    public function resend_otp(Request $request)
    {
        $otp = $request->session()->get('otp');

        $userid = $request->session()->get('userid');
        $user = User::find($userid);

        $res = $this->send_otp($otp, $user->mobile);

        if ($res) {
            return response()->json([
                    'success' => true,
                    'message' => 'OTP resent successfully',
                ]
            );
        }
    }
    public function verify_otp(Request $request)
    {
        $enteredOtp = $request->input('otp');

        $otp = $request->session()->get('otp');
        if ($otp == $enteredOtp) {


        $data = $request->all();
        if(isset($data['full_name'])){
            
        $mobile_num = $request->session()->get('mobile_num'); 
            
        $user = new User;
        $user->full_name = $data['full_name'];
        $user->user_name = '';
        $user->password = '';
        $user->email = $data['email'];
        $user->mobile_num = $mobile_num;
        $user->role_id = 1002;
        $user->otp_verified = 0;
        $res = $user->save();
        $userid=$user->id;
        Session::forget('mobile_num');
        }


        else{
            $userid = $request->session()->get('userid'); 
            Session::forget('userid');
        }


           
            // Updating user's status as 1.
            User::where('id', $userid)->update(['otp_verified' => 1]);

            //Removing Session variable
            Session::forget('otp');
            
            
            Auth::loginUsingId($userid);

            // $intended_url = Session::get('url.intended', url('/product/collection'));

            // Session::forget('url.intended');

            return response()->json([
                    'success' => true,
                    'message' => 'Your Mobile Number is Verified.',
                    'redirect' => url('/')
                ]
            );
        } else {
            return response()->json([
                    'success' => false,
                    'message' => 'OTP does not match.',
                ]
            );
        }
    }

    public function signout()
    {
        Auth::logout();
        return redirect('/');
    }
}
