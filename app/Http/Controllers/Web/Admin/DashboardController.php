<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Booking_detailes;
use App\Ground_detailes;
use Validator;
use DB;
use Auth;

class DashboardController extends Controller
{


  public function price_format($amount){
        setlocale(LC_MONETARY, 'en_IN');
        $amount = money_format('%!i', $amount);
        return $amount;
        }
    public function index()
    {
      $return = [];
        $total=Booking_detailes::where('status', 0)->orderBy('created_at', 'DESC')->count();
     $return['total_booking']=$total;
    $data=Booking_detailes::where('status', 0)->get();

    $p=0;
    foreach ($data as $key ) {
    $p+=$key->price;
    }
        $return['total_lawn'] =Ground_detailes::where('status',0)->count();
          $return['total_vendor'] =\App\Models\Vendor::count();
    $return['total_booking_amount'] =$this->price_format($p);
     // die(json_encode( $return));
        return view('Admin.pages.home', $return);
    }
     public function booking_info()
    {
        return view('Admin.pages.booking_info');
    }

 

   public function  booking_details(Request $request){

     $query = $request->query();
  $response['data'] = Booking_detailes::query()->with('requester','ground')->where('status', 0);
  if (@$query['date_of_visit']) {
  $date_of_visit = date("Y-m-d", strtotime($query['date_of_visit']));
  $response['data']->where('booking_date',$date_of_visit);
  }
  $total=$response['data']->orderBy('created_at', 'DESC')->count();
  $response['data'] =$response['data']->orderBy('created_at', 'DESC')->paginate(10);
  $data= Booking_detailes::where('status', 0)->with('ground')->get();
  $p=0;
  foreach ($data as $key ) {
   $p+=$key->price;
  }
  // foreach ($response['data'] as $key ) {
  // $key->total_amt=number_format($key->total_amt);
  
  // $key->booking_end=date('d/m/Y ',$key->booking_end_date);
  // $key->created=date('d-m-Y', strtotime($key->created_at));
  // }
  $response['total']=$total;
  $response['totalprice']=number_format($p);


   return response()->json($response);

   }

}
