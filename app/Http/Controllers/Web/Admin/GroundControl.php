<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use App\Ground_detailes;
use App\Image_master;
use App\User;
use DateTime;
use Image;

class GroundControl extends Controller
{

public function index(Request $request)
    {

    return view('Admin.pages.grounddetails');
       
    }

    // public function  booking_details(){


    public function booking_details(Request $request){

  $query = $request->query();
  $response['data'] = Booking_detailes::query()->with('requester','ground','match')->where('status', 0);
  if (@$query['date_of_visit']) {
  $date_of_visit = date("Y-m-d", strtotime($query['date_of_visit']));
  $response['data']->where('created_at', 'like',$date_of_visit.'%');
  }
  $total=$response['data']->orderBy('created_at', 'DESC')->count();
  $response['data'] =$response['data']->whereNotIn('id',$res_id)->orderBy('created_at', 'DESC')->paginate(10);
  $data= Booking_detailes::where('status', 0)->with('ground')->get();
  $p=0;
  foreach ($data as $key ) {
   $p+=$key->total_amt;
  }
  foreach ($response['data'] as $key ) {
  $key->total_amt=number_format($key->total_amt);
    $data =Ground_detailes::find($id);
    $key->title=$data->title;
  // $key->booking_end=date('d/m/Y ',$key->booking_end_date);
  $key->created=date('d-m-Y', strtotime($key->created_at));
  }
  $response['total']=$total;
  $response['totalprice']=number_format($p);


   return response()->json($response);
}
   //  $data=Booking_detailes::paginate(10);
   //  return $data;

   // }

public function get_add_details(){
   
        return view('Admin.pages.addground');
}



     public function edit($id) {
     $data =Ground_detailes::find($id);
     $img=Image_master::where('ground_id',$data->id)->where('category','ground_images')->get();
     foreach ($img as $imgpath ) {
     $imgpath->url='/get_image/'.$imgpath->category.'/'.$imgpath->year.'/'.$imgpath->month.'/'.$imgpath->image_name.'.'.$imgpath->ext.'/300~200.png';
     unset($imgpath->ground_id,$imgpath->category,$imgpath->year,$imgpath->month,$imgpath->image_name,$imgpath->ext,$imgpath->created_at,$imgpath->updated_at);
     }
     $data->image=$img;
    return view('Admin.pages.editground', ['ground' => $data]);
     }

     public function get_details(){
        $data =Ground_detailes::where('status',0)->paginate(10);
        foreach ($data as $key) {
          $user=User::find($key->user_id);
          $key->username=$user->user_name;
          $key->created=date('d-m-Y', strtotime($key->created_at));
          $key->priceperday=number_format($key->priceperday);
        }
        return $data;
     }

     public function add(Request $request) {
     // die(json_encode("value"));
       $inputs    = $request->all();
        $rules     = array(
            'title' => 'required',
            'latitude' => 'required', 'price_ground' => 'required', 'description' => 'required', 'user' => 'required', 'best_suited' => 'required','longitude' => 'required'

        );

        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

          $data = new Ground_detailes;
          $data->title=$request->title;
          $data->latitude=$request->latitude;
          $data->best_suited=$request->best_suited;
          $data->priceperday=$request->price_ground;
          $data->description=$request->description;
          $data->user_id=$request->user;
          $data->address=$request->address;
          $data->longitude=$request->longitude;
          $data->save();
          if($data){
          $id = $data->id;
          $files= $request->file('ground_img');
          foreach ($files as $file) {
          $img_cat    = "ground_images";
          $i_year     = date('Y');
          $i_month    = date('m');
          $image_name = uniqid();
          $ext        = $file->getClientOriginalExtension();
          if (!is_dir(storage_path() . "/Images/" . $img_cat)) {
                mkdir(storage_path() . "/Images/" . $img_cat);
            }
            
            if (!is_dir(storage_path() . "/Images/" . $img_cat . "/" . $i_year)) {
                mkdir(storage_path() . "/Images/" . $img_cat . "/" . $i_year);
            }
            if (!is_dir(storage_path() . "/Images/" . $img_cat . "/" . $i_year . "/" . $i_month)) {
                mkdir(storage_path() . "/Images/" . $img_cat . "/" . $i_year . "/" . $i_month);
            }
            
            $image_fullpath = "/Images/" . $img_cat . '/' . $i_year . '/' . $i_month . '/' . $image_name;
            
            list($width, $height) = getimagesize($file);
            
            $ratio = $width / $height; // width/height
            $width  = 800;
            $height = 800;
            
            Image::make($file)->resize($width, $height)->save(storage_path() . $image_fullpath . "." . $ext);
            $imgpath=new Image_master;
           $imgpath->ground_id    = $id;
            $imgpath->category   = $img_cat;
            $imgpath->year       = $i_year;
            $imgpath->month      = $i_month;
            $imgpath->image_name = $image_name;
            $imgpath->ext        = $ext;
            $imgpath->save();
            
        
    }   
 



         return response()->json(array(
                    'success' => true,
                    'message' => "Ground  Added successfully.",
                    'id'=>$data->id,
                    'name'=>$data->name
                    ));
     }



    }


      public function update(Request $request,$id) {
     // die(json_encode("value"));
       $inputs    = $request->all();
        $rules     = array(
            'title' => 'required',
            'latitude' => 'required', 'price_ground' => 'required', 'description' => 'required', 'user' => 'required', 'best_suited' => 'required','longitude' => 'required','address'=>'required'

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

          $data =Ground_detailes::find($id);
          $data->title=$request->title;
          $data->latitude=$request->latitude;
          $data->best_suited=$request->best_suited;
          $data->priceperday=$request->price_ground;
          $data->description=$request->description;
          $data->user_id=$request->user;
           $data->address=$request->address;
          $data->longitude=$request->longitude;
          $data->save();
        
          if($request->image_id){
            $files= $request->file('ground_img');
          if($files){  
        foreach ($files  as $key => $file){
 
        if($file){
          $imgpath=Image_master::find($request->image_id[$key]);
          if ($imgpath) {
           $image_path1 = storage_path("/Images/" . $imgpath->category . "/" . $imgpath->year . "/" . $imgpath->month . "/" . $imgpath->image_name . "." . $imgpath->ext);
    
          
                if (File_exists($image_path1)) {
                    unlink($image_path1);
                 }
          
            $img_cat    = "ground_images";
            $i_year     = date('Y');
            $i_month    = date('m');
            $image_name = uniqid();
            $ext        = $file->getClientOriginalExtension();

            if (!is_dir(storage_path() . "/Images/" . $img_cat)) {
                mkdir(storage_path() . "/Images/" . $img_cat);
            }
            
            if (!is_dir(storage_path() . "/Images/" . $img_cat . "/" . $i_year)) {
                mkdir(storage_path() . "/Images/" . $img_cat . "/" . $i_year);
            }
            if (!is_dir(storage_path() . "/Images/" . $img_cat . "/" . $i_year . "/" . $i_month)) {
                mkdir(storage_path() . "/Images/" . $img_cat . "/" . $i_year . "/" . $i_month);
            }
            
            $image_fullpath = "/Images/" . $img_cat . '/' . $i_year . '/' . $i_month . '/' . $image_name;
            
            list($width, $height) = getimagesize($file);
            
            $ratio = $width / $height; // width/height
            $width  = 800;
            $height = 800;
            
            Image::make($file)->resize($width, $height)->save(storage_path() . $image_fullpath . "." . $ext);
            
           $imgpath->ground_id    = $data->id;
            $imgpath->category   = $img_cat;
            $imgpath->year       = $i_year;
            $imgpath->month      = $i_month;
            $imgpath->image_name = $image_name;
            $imgpath->ext        = $ext;
            $imgpath->save();
        }

      }
    }
  }
  }
         
    return response()->json(array(
    'success' => true,
    'message' => "Ground  Updated successfully."
     ));
     



    }

    public function delete($id){
    	$data =Ground_detailes::find($id);
    	$data->status=1;
      $data->save();

    	if($data){
    		return response()->json(array(
                    'success' => true,
                    'message' => "Floor  Delete successfully."));
    	}

    }

}