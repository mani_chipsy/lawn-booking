<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class GetUserFromToken extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
       $headers = apache_request_headers();
       if (isset($headers['Authorizations'])) {
       $token= $headers['Authorizations'];
        try {
           $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            $response['status']["code"] = 2024;
            $response['status']["message"] = 'Session expired.';
            return response()->json($response);
        } catch (JWTException $e) {
            $response['status']["code"] = 1024;
            $response['status']["message"] = 'Invalid Authorizations.';
            return response()->json($response);
        }
        if (!$user) {
            return $this->respond('tymon.jwt.user_not_found', 'user_not_found', 404);
        }
        $this->events->fire('tymon.jwt.valid', $user);
        // $GLOBALS['user']=$user;
        return $next($request);
       }
      else{
        $response['status']["code"] = 1023;
        $response['status']["message"] = 'Authorizations not found.';
        return response()->json($response);
      }
    }
}