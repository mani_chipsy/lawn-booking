<?php

use Illuminate\Database\Seeder;

class PrivacySettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Dummy Entries starts
        // $settings = DB::collection('privacy_settings')->where('status', 1)->get();
        // $users = DB::collection('users')->get();

        // foreach ($users as $value) {
        //     foreach ($settings as $sett) {
        //         DB::collection('user_privacies')->insert([
        //             'user_id'      => (string)$value['_id'],
        //             'privacy_setting_id'  => (string)$sett['_id'],
        //             'checked'       => 1
        //         ]);
        //     }
        // }
        // Dummy entires ends
        
        DB::table('privacy_settings')->insert([
            'description' => 'Show my display name',
            'status' => 1,
        ]);
        
        DB::table('privacy_settings')->insert([
            'description' => 'Show my birth date',
            'status' => 1,
        ]);

        DB::table('privacy_settings')->insert([
            'description' => 'Show my email',
            'status' => 1,
        ]);

        DB::table('privacy_settings')->insert([
            'description' => 'Show my online status on chat',
            'status' => 1,
        ]);
    }
}
