<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>Login | Admin Panel</title>

  <link rel="apple-touch-icon" href="/backend/assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="/backend/assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="/backend/css/bootstrap.min.css">
  <link rel="stylesheet" href="/backend/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="/assets/css/site.min.css">
<style type="text/css">.parsley-errors-list.filled {
    opacity: 1;
    text-align: left;
}</style>
  <!-- Skin tools (demo site only) -->

  <!-- Plugins -->

  <!-- Page -->
  <link rel="stylesheet" href="/backendassets/examples/css/pages/login-v3.min.css">

  <!-- Fonts -->
  <link rel="stylesheet" href="/backend/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="/backend/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic">

<link rel="stylesheet" type="text/css" href="{{asset('/backend/assets/parsley/parsley.css')}}">
 <script src="/backend/assets/js/jquery.min.js"></script>
         <script src="/backend/assets/lib/angular/angular.min.js"></script>
        <script src="/backend/assets/lib/angular/route.js"></script>
  
  
</head>
<body class="animsition site-navbar-small page-login-v3 layout-full" ng-app="TapmiAdmin" >
          
        <div class="login-box" ng-controller="LoginController">   


  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
      <div class="panel">
        <div class="panel-body">
          <div class="brand">
            <img class="brand-img" src="../backend/assets/images/logo-colored.png" alt="...">
            <h2 class="brand-text font-size-18">Remark</h2>
          </div>
           <form id="sign_in" method="POST" action=""  name="login-inputs" >
                        <input type="hidden" name="_token" value="1Vg9ztkT4d7d7srf9BeKDjxUy3Baba0qU7f0QMn7">  
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" class="form-control" placeholder="Username" name="username" required="true" autofocus />
              <!-- <label class="floating-label">User Name</label> -->
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control" placeholder="Password" name="password" required="true" />
            <!--   <label class="floating-label">Password</label> -->
            </div>

                        <div class="col-xs-8 p-t-5">          
                                <div name="message_area">
                                    <label></label>
                                </div>  
                            </div>
           <!--  <div class="form-group clearfix">
              <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                <input type="checkbox" id="inputCheckbox" name="remember">
                <label for="inputCheckbox">Remember me</label>
              </div>

            </div> -->
            <button type="button" class="btn btn-primary btn-block btn-lg mt-40" name="submit-login">Sign in</button>
          </form>
          
        </div>
      </div>

      
    </div>
  </div>
  </div>
  <!-- End Page -->


  <!-- Core  -->

  <script src="/backend/jscontroller/app_controls.js"></script>

  <script src="/backend/vendor/jquery/jquery.min.js"></script>
  <script src="/backend/vendor/popper-js/umd/popper.min.js"></script>
  <script src="/backend/vendor/bootstrap/bootstrap.min.js"></script>
   <script src="{{asset('/backend/assets/parsley/parsley.js')}}"></script>
      <script src="/backend/jscontroller/login_controls.js"></script>
<!--   <script src="/backend/vendor/animsition/animsition.min.js"></script> -->
<!--   <script src="/backend/global/vendor/mousewheel/jquery.mousewheel.js"></script> -->


  <!-- Google Analytics -->
  
</body>

</html>