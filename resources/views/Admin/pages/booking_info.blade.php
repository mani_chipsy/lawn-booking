 @extends('Admin.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/backend/assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">
 <div class="page" ng-controller="BookingController" ng-cloak>
 <div class="page-content container-fluid" >
    <div class="page-header">
      <h1 class="page-title"> Booking Details</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/superadmin/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Booking Details</li>
      </ol>
    <div class="page-header-actions">
    <label for="search" class="small-label">Search: </label>
             <input type="text" id="datepicker"  class="form-control date" value="" name="date_of_visit" title="Date of Booking" placeholder="Date of Booking"   style="width: 200px;    display: inline;">
             <button type="button" class="btn btn-sm btn-primary btn-round waves-effect waves-classic waves-effect waves-classic" id="search">Search </button>
      </div>
    </div>

 



  

      <!-- /.row -->
     <!--  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                 <div class="header form-inline">
               <h2>
                            <span>Total Ground( <%totalUsers%> )</span>
                            <p class="pull-right">
                              

                                 <a href="/superadmin/ground-create" class="btn btn-info " id="submit">Add New Ground</a>

                            </p>
                        </h2>
              </div> -->
<!-- 
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <a href="/superadmin/ground-create" class="btn btn-info pull-right" id="submit">Add New Ground</a>
                  </div>
                </div> -->
             <div class="col-md-12" id="editdisable">
       <div class="panel">
          <div class="panel-body container-fluid">
        
       <div class="example-wrap">
     
               <h4 class="example-title">Total Booking ( <%totalUsers%> ) <span style="float: right">Total Amount ( Rs. <%totalAmount%> ) </span> </h4>
           
     
                 
         
            <!-- /.box-header -->
             <div class="example table-responsive">
                  <table class="table">
                <tbody>
                <tr>
                  <th>ID</th>
                  <th>Lawn Name</th>
                  <th>User Name</th>
             <!--      <th>Latitude</th> -->
                  <th>Description</th>
                  <th>Booking Date</th>
   
                  <th>Price</th>
                  
                  
                <!--   <th colspan="2">Action</th> -->
                </tr>
               
                <tr ng-repeat="ground in grounds" ng-cloak>
                                  <th scope="row" width="5%"><%(usersPerPage * (currentPage-1)) + $index+1  %></th>
                                   <td  title="<% ground.ground.title %>" width="20%"><% ground.ground.title %> </td>
                                    <td  width="10%"><% ground.requester.full_name %> </td>
                                
                                 
                                   
                                    <td width="35%"> <% ground.description%></td>
                                      <td><% ground.booking_date %></td>
                                   <td> <% ground.price %></td>
                                                             
                                    
                                    
                                   <!--  <td style="width: 10px;"> 

            <a href="/superadmin/ground/edit/<%ground.id%>" class="btn btn-icon btn-info waves-effect waves-classic"><i class="icon md-edit" aria-hidden="true"></i>
                        </a>



                                    </td>
                                    <td >
                            <button id="<%ground.id%>" class="btn btn-icon btn-danger waves-effect waves-classic" data-ng-click="delete($event)">
                           <i class="icon md-delete" aria-hidden="true"></i>
                        </button>
                      
             

                                        </td> -->
                                </tr>
                                    <tr ng-hide="grounds.length">
                                   <td colspan="11" align="center" style="color: red"><b>No Booking Details to display</b></td>
                                </tr> 
              </tbody></table>
              
            </div>
            <div class="box-footer clearfix">
             <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalUsers" ng-model="currentPage" class="pagination pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="usersPerPage"></ul>
                          </div>
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
</div>
</div>
</div>


<script src="{{asset('/backend/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('backend/jscontroller/create.js')}}"></script>
<script type="text/javascript">

    $('#datepicker').datepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    orientation: 'bottom',
   
    minView: 2,
     format: 'yyyy-mm-dd',
     // startDate: new Date(),
    forceParse: 0
    });






</script>


  @endsection