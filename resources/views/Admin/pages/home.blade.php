
 @extends('Admin.layouts.master_layout')
@section('content')
<div class="page">
    <div class="page-content container-fluid">
      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea One-->
          <div class="card card-shadow" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                   <i class="icon md-flash grey-600 font-size-24 vertical-align-bottom mr-5"></i>     Total Lawn Details
                </div>
                <span class="float-right grey-700 font-size-30">{{$total_lawn}}</span>
              </div>
            
              <div class="ct-chart h-50"></div>
            </div>
          </div>

          <!-- End Widget Linearea One -->
        </div>
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea Two -->
          <div class="card card-shadow" id="widgetLineareaTwo">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                    <i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i>          Vendor
                </div>
                <span class="float-right grey-700 font-size-30">{{$total_vendor}}</span>
              </div>
           
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea Two -->
        </div>
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea Three -->
          <div class="card card-shadow" id="widgetLineareaThree">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>                  Total Amount
                </div>
                <span class="float-right grey-700 font-size-30">{{$total_booking_amount}}</span>
              </div>
            
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea Three -->
        </div>
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea Four -->
          <div class="card card-shadow" id="widgetLineareaFour">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-view-list grey-600 font-size-24 vertical-align-bottom mr-5"></i> Total Booking
                </div>
                <span class="float-right grey-700 font-size-30">{{$total_booking}}</span>
              </div>
           
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea Four -->
        </div>

      
          <!-- End Panel Projects Stats -->
        </div>

      </div>
    </div>

  @endsection