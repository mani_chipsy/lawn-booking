<!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2018 <a href=" "> garden </a></div>
    <div class="site-footer-right">
      Crafted with <i class="red-600 wb wb-heart"></i> by <a target="_blank" href="http://chipsyservices.com">chipsyservices</a>
    </div>
  </footer>
  <!-- Core  -->
 <script src="/backend/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="/backend/vendor/jquery/jquery.min.js"></script>
  <script src="/backend/vendor/popper-js/umd/popper.min.js"></script>
  
  <script src="/backend/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="/backend/vendor/animsition/animsition.min.js"></script>
  <script src="/backend/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="/backend/vendor/asscrollbar/jquery-asScrollbar.min.js"></script>
  <script src="/backend/vendor/asscrollable/jquery-asScrollable.min.js"></script>
  <script src="/backend/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>
  <script src="/backend/vendor/waves/waves.min.js"></script>
 <script src="{{asset('/backend/assets/parsley/parsley.js')}}"></script>
  <!-- Plugins -->
  <script src="/backend/vendor/switchery/switchery.min.js"></script>
  <script src="/backend/vendor/intro-js/intro.min.js"></script>
  <script src="/backend/vendor/screenfull/screenfull.min.js"></script>
  <script src="/backend/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <!-- Plugins For This Page -->
<!--   <script src="/backend/vendor/chartist/chartist.min.js"></script>
  <script src="/backend/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js"></script> -->
  <script src="/backend/vendor/jvectormap/jquery-jvectormap.min.js"></script>
  <script src="/backend/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
  <script src="/backend/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="/backend/vendor/peity/jquery.peity.min.js"></script>
<script src="/backend/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
  <script src="/backend/vendor/dropify/dropify.min.js"></script>


    <script src="/backend/vendor/jquery-ui/jquery-ui.min.js"></script>
  <script src="/backend/vendor/moment/moment.min.js"></script>
  <script src="/backend/vendor/fullcalendar/fullcalendar.min.js"></script>
  <script src="/backend/vendor/jquery-selective/jquery-selective.min.js"></script>
  <script src="/backend/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="/backend/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js"></script>
  <script src="/backend/vendor/bootbox/bootbox.min.js"></script>


  <script src="/backend/js/State.min.js"></script>
  <script src="/backend/js/Component.min.js"></script>
  <script src="/backend/js/Plugin.min.js"></script>
  <script src="/backend/js/Base.min.js"></script>
  <script src="/backend/js/Config.min.js"></script>

  <script src="/backend/assets/js/Section/Menubar.min.js"></script>
  <script src="/backend/assets/js/Section/Sidebar.min.js"></script>
  <script src="/backend/assets/js/Section/PageAside.min.js"></script>
  <script src="/backend/assets/js/Plugin/menu.min.js"></script>

  <!-- Config -->
  <script src="/backend/js/config/colors.min.js"></script>
  <script src="/backend/assets/js/config/tour.min.js"></script>
<!--   <script>
    Config.set('assets', 'assets');
  </script> -->

  <!-- Page -->
  <script src="/backend/vendor/aspaginator/jquery-asPaginator.min.js"></script>
  <script src="/backend/assets/js/Site.min.js"></script>
  <script src="/backend/js/Plugin/asscrollable.min.js"></script>
  <script src="/backend/js/Plugin/slidepanel.min.js"></script>
  <script src="/backend/js/Plugin/switchery.min.js"></script>


  <script src="/backend/assets/examples/js/uikit/carousel.min.js"></script>
  <script src="{{asset('/backend/assets/jquery-confirm/jquery-confirm.js')}}"></script>

<!--      <script src="http://127.0.0.1:8888/backend/assets/plugins/jquery/jquery.twbsPagination.js"></script> -->
  <script src="/backend/assets/examples/js/dashboard/v1.min.js"></script>

   <script src="/backend/assets/sweetalert/sweetalert.min.js"></script>

