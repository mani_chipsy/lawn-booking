
 @extends('Vendor.layouts.master_layout')
@section('content')
<div class="page">
    <div class="page-content container-fluid">
     

        <h1>Grounds</h1>
         <div class="row" data-plugin="matchHeight" data-by-row="true">
           @foreach ($lawn as $res)
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea One-->
          <a href="/vendor/lawn/booking/{{$res->id}}" style="text-decoration: none">
          <div class="card card-shadow" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i>              {{$res->title}}
                </div>
                <span class="float-right grey-700 font-size-30">{{$res->total}}</span>
              </div>
              <div class="mb-20 grey-500">
                <i class="icon md-long-arrow-up green-500 font-size-16"></i> {{$res->description}}
              </div>
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          </a>
          <!-- End Widget Linearea One -->
          </div>
           @endforeach
          </div>

      </div>
    </div>

  @endsection