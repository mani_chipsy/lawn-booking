 @extends('Admin.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('admin/parsley/parsley.css')}}">



<div class="page">
    <div class="page-header">
      <h1 class="page-title">Settings</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/vendor/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Settings</li>
      </ol>
    
    </div>

    <div class="page-content container-fluid" ng-controller="SettingsController">
      <div class="row">
      
        <div class="col-md-5">
          <!-- Panel Static Labels -->
            <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Change Username/ Password</h3>
            </div>


            <div class="panel-body container-fluid">

        <form  name='update_profile'>
            {{csrf_field()}}
          
          
                        <div class="body">
                            <label for="username">Username</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="username" id="username" required="required" name="username" class="form-control" placeholder="Username" value="{{Auth::user()->name}}" data-parsley-errors-container="#username_error">
                                </div>
                                <div id="username_error"></div>
                            </div>
                            <input type="checkbox" id="change_pass" name="change_pass" value="1" class="filled-in">
                            <label for="change_pass">Change Password</label><br />
                            <label for="current_password">Current Password</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" id="current_password" required="required" name="current_password" class="form-control" placeholder="Current Password" data-parsley-errors-container="#old_pass_error">
                                </div>
                                <div id="old_pass_error"></div>
                            </div>
                            <div name="checkd-chg-pass" >
                            </div>
                            <div class="resp-msg"></div>
                            <button type="button" name="updateProfile" class="btn btn-success m-t-15 waves-effect">Submit</button>
                        </div>
                    </div>
             
                </form>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
<script src="{{asset('admin/jscontrols/settings_controls.js')}}"></script>




@endsection
