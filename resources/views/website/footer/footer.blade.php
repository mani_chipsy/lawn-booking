
	<script type="text/javascript" src="/website/assets/js/jquery.min.js"></script>
	<script src="/website/assets/js/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/website/assets/js/tether.min.js"></script>
	<script type="text/javascript" src="/website/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/website/assets/js/owl.carousel.js"></script>
	<!-- fancybox Js -->
	<script src="/website/assets/js/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script>
	<script src="/website/assets/js/jquery.fancybox.pack.js" type="text/javascript"></script>
	<!-- masonry,isotope Effect Js -->
	<script src="/website/assets/js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
	<script src="/website/assets/js/isotope.pkgd.min.js" type="text/javascript"></script>
	<script src="/website/assets/js/masonry.pkgd.min.js" type="text/javascript"></script>
	<script src="/website/assets/js/jquery.appear.js" type="text/javascript"></script>
	<!-- revolution Js -->
	<script type="text/javascript" src="/website/assets/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="/website/assets/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="/website/assets/js/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="/website/assets/js/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="/website/assets/js/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="/website/assets/js/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="/website/assets/js/jquery.revolution.js"></script>
	<!-- Mail Function Js -->
	<script type="text/javascript" src="/website/assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
	<script src="/website/assets/js/validation.js" type="text/javascript"></script>
	<!-- Compiled and minified JavaScript -->
	<script type="text/javascript" src="/website/assets/js/jquery.prettyPhoto.js"></script>
	<script src="/website/assets/js/jquery.appear.js" type="text/javascript"></script>
	<script src="/website/assets/js/custom.js" type="text/javascript"></script>
<script src="{{asset('/backend/assets/parsley/parsley.js')}}"></script>
	  <script src="/website/js/fileuploade.js"></script>
	  
	<script src="/website/js/auth.js"></script>
	
