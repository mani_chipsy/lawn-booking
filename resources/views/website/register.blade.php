 <div id="preloader" style="display: none">
            <div id="status" style="display: none"></div>
        </div>
<link rel="stylesheet" type="text/css" href="{{asset('assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/jquery-confirm/jquery-confirm.css')}}">
@extends('Frontend.layouts.master_layout')
@section('content')
  
<style type="text/css">
    .wrapper {
    min-height: 84%;
    position: relative;
}
</style>
    <!-- CONTENTS -->

     <!--    <div class="row" style="margin: 0px;">   -->
    <div class="container">   <!-- /container ===================== -->


        <!-- <div class="breadcrumbs">
            <a href="/site/home">Home</a> » <span>Suggestions</span>
        </div> -->
        <div class="row">

           <div class="col-md-12 logofinal"  >
                <a href="/" style="text-align: left !important;"> <img name="" src="Frontend/img/logo.png" alt=" Eximter "   id="" > </a>
           </div>
        </div>


        <div class="row">
            <div class="col-sm-12 col-md-12">
                <ul class="breadcrumb">
 
  <li><a href="/" title="Eximter" temprop="url"><span itemprop="title">Home</span></a>
                           </li>
                          <li>
                          <a href="" title="" itemprop="url"><span itemprop="title">Login </span> </a> </li>
                       

                    
  
                </ul>

               
            </div>

        </div> 
        
         <div class=" col-xs-12 col-sm-10 col-md-8 col-center" style="margin-top:-40px; border:0px solid;">
               
                
                
                    <div class="page-header " id="pagehead" style="width:100%">
                
                
                   <h2>Login <br><small>Don't have an account yet ? <a href="/register">Signing up</a> is Free !</small></h2>
                </div>
               
            </div>  
        </div>

      <br>
            <div class="container" ng-controller="LoginController">   <!-- /container ===================== -->
                 <div class="row" style="margin: 0px;"  >  
                <div class="col-lg-6 col-xs-10 col-sm-6 col-md-8 col-center " style="min-height:400px ">
                    <form class="form-horizontal" id="customer_login" ng-enter="login()"  name="customer_login" method="post" action="">
                    
                      {{csrf_field()}}
                      

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2  control-label" for="Email">Email<span class="required" style="color:red;">*</span></label>  
                            <div class="col-md-7">
                                <input required="required" type="email" value="" name="email" id="customer_email" value="" placeholder="Enter E-mail address"  class="form-control input-md" required  autocomplete="off">
                                <span class="usernameError" style="color:red;"></span>

                            </div>
                        </div>

                        <!-- Password input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="password">Password<span class="required" style="color:red;">*</span></label>
                            <div class="col-md-7">
                                <input type="password"  required="required" value="" name="password" id="password" value="" placeholder="Enter password" class="form-control input-md"  autocomplete="off">
                                <span class="passwordError" style="color:red;"></span>
                                <div class="details">  
                                                
                                        <input name="rememberMe" id="rememberMe" value="1" type="checkbox"> Remember Me                 
                                            
<!--                                            <span style="color:red;"> <div class="errorMessage" id="LoginForm_rememberMe_em_" style="display:none"></div></span> -->

                                </div>
                                <div class="details">

                                    <a href="/forgotpassword">Forgot your Password?</a>

                                </div>

                                <div class="resp-msg" style="    color: red;
    text-align: center;
    margin-bottom: 10px; font-weight: 900; " ></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-10 text-center">
                                <input class="btn btn-primary " type="button" value="Sign In" ng-click="login()">         
                                <br>  <span class="errorMessage" style="color:red;"></span>
                            </div>
                        </div>

                        <div class="form-group">
                                                    </div>

                    </form>
                      






                </div> <!-- /container -->
            </div>

        </div>  
        <!-- CONTENTS END -->

  <style type="text/css">
          @media screen and (min-width:1024px){#captchaLtr{width:303px}}
          @media screen and (min-width:480px) and (max-width:768px){#captchaLtr{width:187px !important}}  
          @media screen and (min-width:1280px){
.form-horizontal{margin-left:128px !important}
.col-lg-6{width:64% !important}

}         
              </style>
  
    <script src="{{asset('Frontend/user/register.js')}}"></script>
<script src="{{asset('assets/parsley/parsley.js')}}"></script>
<script src="{{asset('assets/jquery-confirm/jquery-confirm.js')}}"></script>
   @endsection


