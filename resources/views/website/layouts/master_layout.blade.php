<!DOCTYPE html>
<html lang="en">
 @include('website.header.header')

  
   <body>
    <!--loader-->
    <div id="preloader">
        <div class="sk-circle">
            <div class="sk-circle1 sk-child"></div>
            <div class="sk-circle2 sk-child"></div>
            <div class="sk-circle3 sk-child"></div>
            <div class="sk-circle4 sk-child"></div>
            <div class="sk-circle5 sk-child"></div>
            <div class="sk-circle6 sk-child"></div>
            <div class="sk-circle7 sk-child"></div>
            <div class="sk-circle8 sk-child"></div>
            <div class="sk-circle9 sk-child"></div>
            <div class="sk-circle10 sk-child"></div>
            <div class="sk-circle11 sk-child"></div>
            <div class="sk-circle12 sk-child"></div>
        </div>
    </div>

    <!--loader-->
    <!--Header Section Start Here
        ==================================-->
    <header>
        <div class="top-part__block">
            <div class="search__box-block">
                <div class="container">
                    <input type="text" id="search" class="input-sm form-full" placeholder="Search Now">
                    <a href="#!" class="search__close-btn">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <p>
                            Welcome to our corporate buisness
                        </p>
                    </div>
                    <div class="col-sm-5">
                        <div class="social-link__block text-right">
                            <a href="#" class="facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" class="twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#" class="google-plus">
                                <i class="fa fa-google-plus"></i>
                            </a>
                            <a href="#" class="linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="middel-part__block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="logo">
                            <a href="/">
                                <img src="/website/assets/images/logo.jpg" alt="Logo"> </a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12">
                        <div class="top-info__block text-right">
                            <ul>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <p>
                                        13005 Greenvile Avenue
                                        <span> California, TX 70240</span>
                                    </p>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <p>
                                        Call Us
                                        <span> +56 (0) 012 345 6789</span>
                                    </p>
                                </li>
                                <li>
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <p>
                                        Mail Us
                                        <span>
                                            <a href="mailto:info@gmail.com">info@gmail.com</a>
                                        </span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_nav stricky-header__top navbar-toggleable-md">

            <nav class="navbar navbar-default navbar-sticky bootsnav">
                <div class="container">
                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle hidden-lg-up" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>

                    </div>
                    <!-- End Header Navigation -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav mobile-menu">
                            <li>
                                <a href="/">Home</a>
                               
                        <!--     <li>
                                <a href="#!">About us</a>
                                <span class="submenu-button"></span>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="about.html">About</a>
                                    </li>
                                    <li>
                                        <a href="about2.html">About 2</a>
                                    </li>
                                    <li>
                                        <a href="about3.html">About 3</a>
                                    </li>
                                    <li>
                                        <a href="history.html">history</a>
                                    </li>
                                    <li>
                                        <a href="career.html">career</a>
                                    </li>
                                    <li>
                                        <a href="partnerships.html">partnerships</a>
                                    </li>
                                    <li>
                                        <a href="leadership.html">leadership</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:avoid(0);">Services</a>
                                <span class="submenu-button"></span>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="services.html">All Services</a>
                                    </li>
                                    <li>
                                        <a href="services-details.html">Services Details</a>
                                    </li>

                                </ul>
                            </li> -->
                              <?php $login=0;?>
                               <li style="float:right;" id="loc" ><a href='#locationModal' data-toggle="modal" ><i class="fa fa-map-marker" style="    padding-right: 5px;"></i> 
                               @if(session()->get('location'))
                               {{session()->get('location')}}
                               @else
                               Udupi
                               @endif </a></li>
                            <?php $auth=isset(Auth::user()->id);?>
                          
                @if($auth)
                <?php $login=1;?>
                
                
                <li style="float:right;"><a href="/signout">Logout</a></li>
                <li style="float:right;padding-top: 20px;"> Name: {{Auth::user()->full_name }} </li>
                
                 @else
            <!--        <li style="float:right;"><a href="#createAccount" data-toggle="modal">Register</a></li> -->
                <li style="float:right;" id="login_ischnage"><a href='#loginModal' data-toggle="modal" >Login / Signup</a></li>
              
                            @endif
                            
                    <input type="text" hidden="true" name="login" id="is_login" value="{{$login}}" >
                         
                        </ul>
                    </div>
                    <!--navbar-collapse -->
                </div>
            </nav>
        </div>
    </header>
        <!-- CONTENTS -->

        <!-- <hr> -->
         
        
       @yield('content')
     

    
        <!-- /.container -->
 
<style type="text/css">
    .divider{
         border-left: 1px solid #000000;
    }
</style>
<footer>
        <div class="top_footer_info__block ptb-20">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="single_info__block">
                            <i class="fa fa-phone"></i>
                            <h4>0(000) 000 000
                                <span>Monday-Friday, 8am-7pm</span>
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single_info__block">
                            <i class="fa fa-envelope-o"></i>
                            <h4>business@support.com
                                <span>Monday-Friday, 8am-7pm</span>
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single_info__block">
                            <i class="fa fa-bullhorn"></i>
                            <h4>Request a quote
                                <span>Get all the information</span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_footer__block pb-0 pt-60 pt-xs-40">
            <div class="container">
                <div class="row mb-60">
                    <div class="col-lg-3">
                        <div class="footer_box__block">
                            <h4>About Us</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, architecto, asperiores.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid sequi, fuga rem aperiam expedita ipsum.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3 mt-sm-30 mt-xs-30">
                        <div class="footer_box__block">
                            <h4>Latest Blog Post</h4>
                            <ul>
                                <li>
                                    <a href="#">Start your own agency</a>
                                </li>
                                <li>
                                    <a href="#">How to cool down quality</a>
                                </li>
                                <li>
                                    <a href="#">Make something awesome</a>
                                </li>
                                <li>
                                    <a href="#">Plane your summer vacation</a>
                                </li>
                                <li>
                                    <a href="#">There are sunlight to enjoy</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 mt-xs-30">
                        <div class="footer_box__block">
                            <h4>Our Project</h4>
                            <ul>
                                <li>
                                    <a href="#">Go get an ice-cream</a>
                                </li>
                                <li>
                                    <a href="#">Become the best version</a>
                                </li>
                                <li>
                                    <a href="#">Eat, Sleep and have fun</a>
                                </li>
                                <li>
                                    <a href="#">Start the journy to the top</a>
                                </li>
                                <li>
                                    <a href="#">Can you do this for us?</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 mt-sm-30 mt-xs-30">
                        <div class="footer_box__block address-box">
                            <h4>Contact info</h4>
                            <ul>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <p>
                                        Call Us +56 (0) 012 345 6789
                                    </p>
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i>
                                    <p>
                                        <a href="mailto:info@example.com">business@support.com</a>
                                    </p>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <p>
                                        123 Main Street, St. NW Ste, 1 Washington, DC,USA.
                                    </p>
                                </li>
                                <li>
                                    <i class="fa fa-clock-o"></i>
                                    <p>
                                        Monday-Firday, 9am-6pm;
                                        <br> Sunday Closed
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="copyriight_block ptb-20 mt-20">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="#" class="footer__block-logo">
                                <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/footer_logo.png" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <p>
                                All Rights Reserved
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </footer>

@include('website.footer.footer')
 
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <form class="form-horizontal ng-pristine ng-valid" action="/login_post"  method="POST" role="form">
               <fieldset>
             
                       <div class="form-group">
                        <div class="ui-input-group">
                          <input required type="text" name="mobile_num" class="form-control" placeholder="">
                          <span class="input-bar"></span>
                          <label>Mobile Number</label>
                        </div>
                      </div>
         
                <div class="form-group formField" style="text-align: center;">
                  <input type="button" name="create_account" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Login" style="width: 200px">
                </div>
                <div class="form-group formField">
                  <p class="help-block signup-error"> </p>
                </div>
               
                </fieldset>
              </form>
      </div>
      
  </div>
</div>
</div>


<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Search Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="widget-search pt-15">
     
                <input type='text' id="autocomplete" name="loc" required="true" placeholder='Enter a location'  class="login__input form-full input-lg" autocomplete="on" runat="server">
            
            </div>
    <form class="form-horizontal ng-pristine ng-valid" action=""  method="POST" role="form">
               <fieldset>
             
                 <!--       <div class="form-group">
                        <div class="ui-input-group">
                          <input required type="text" name="mobile_num" class="form-control" placeholder="">
                          <span class="input-bar"></span>
                        <label>Mobile Number</label>
                        </div>
                      </div> -->
      
        



                <div class="form-group formField">
                  <p class="help-block signup-error"> </p>
                </div>
               
                </fieldset>
              </form>
      </div>
      
  </div>
</div>
</div>



<style type="text/css">
.modal-body{overflow-y: inherit;}

.modal-open .dropdown-menu {
  z-index: 2050;
}

.ui-front { z-index: 9999; }
.pac-container { z-index: 9999999999; }
</style>
    <!-- ====================================
    ——— CREATE ACCOUNT MODAL
    ===================================== -->
    <div class="modal fade " id="createAccount" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Register</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
              <form class="form-horizontal ng-pristine ng-valid" action="/submit-signup"  method="POST" role="form">
               <fieldset>
                <div class="form-group">
                        <div class="ui-input-group">
                          <input required type="text" name="full_name" class="form-control" placeholder="">
                          <span class="input-bar"></span>
                        <label>Name</label>
                        </div>
                      </div>


                       <div class="form-group">
                        <div class="ui-input-group">
                            <input required type="email" name="email" class="form-control" placeholder="">
                          <span class="input-bar"></span>
                        <label>Email</label>
                        </div>
                      </div>
                       <div class="form-group">
                        <div class="ui-input-group">
                          <input required type="text" name="mobile_num" class="form-control" placeholder="">
                          <span class="input-bar"></span>
                        <label>Mobile Number</label>
                        </div>
                      </div>
         
                <div class="form-group formField" style="text-align: center;">
                  <input type="button" name="create_account" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Register" style="width: 200px">
                </div>
                <div class="form-group formField">
                  <p class="help-block signup-error"> </p>
                </div>
               
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>

      
    </div>
    <div class="modal fade customModal" id="otp_verify_mod" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          
        </div>
      </div>
    </div>

  <style type="text/css">
  .modal {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 9999999;
    display: none;
  }
  .ui-autocomplete{
    z-index:99999999999999!important;
  }
</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDYnBHNVSrYhl5JiPUAsjrz3XO55-LvW1g&libraries=places"></script>
<!-- <script src="http://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script> -->
<!-- <script src="jquery.city-autocomplete.min.js" type="text/javascript"></script> -->
<script>

 var input = document.getElementById('autocomplete');
   var autocomplete = new google.maps.places.Autocomplete(input);
   
   
   google.maps.event.addListener(autocomplete, 'place_changed', function () {
     var place = autocomplete.getPlace();
     // $('#loc1').html('  <i class="fa fa-map-marker fa-lg" aria-hidden="true" ></i>'+place.name);
     // document.getElementById('city2').value = place.name;
     // document.getElementById('cityLat').value = place.geometry.location.lat();
     // document.getElementById('cityLng').value = place.geometry.location.lng();
       var postdata = new FormData();
     postdata.append('city', place.name);
     postdata.append('latitude', place.geometry.location.lat());
     postdata.append('longitude', place.geometry.location.lng());
     $('#loc').html('<a href="#locationModal" data-toggle="modal"><i class="fa fa-map-marker" style="    padding-right: 5px;"></i> '+place.name+'</a>');
      $.ajax({
         url: "find-location",
         async: !1,
         type: "POST",
         dataType: "json",
         contentType: false, // The content type used when sending data to the server.
         cache: false, // To unable request pages to be cached
         processData: false,
         data: postdata,
         success: function(response) {

            var data="";
            if(response.length){
            for(i=0;i<response.length;i++){
            data+='<div class="col-lg-4 col-md-6 full-wid"><div class="project-item">'+
                  '<div class="about-block clearfix"><figure><a href="/lawn/'+response[i].id+'">'+
                  '<img class="img-responsive" src="'+response[i].images[0].url+'" alt="Photo"></a>'+
                  '</figure> <div class="text-box mt-25"> <div class="box-title mb-15">'+
                  '<h3><a href="/lawn/'+response[i].id+'">'+response[i].title+'</a></h3></div><div class="text-content">'+
                  '<p>'+response[i].address+'</p>'+
                  '<a href="/lawn/'+response[i].id+'" class="btn-text  mt-15">Read More</a>'+
                 '</div></div></div></div></div>';
                 
     }
     $('#lawn_append').html(data);
    }

    else
    {
        data+='<div class="col-lg-12 col-md-12 full-wid" style="    text-align: center;">'+
          '<img class="img-responsive" src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_6.jpg" alt="Photo"></div>';
        $('#lawn_append').html(data);

    }
    
      $('.modal-backdrop').hide(); // for black background
      $('body').removeClass('modal-open'); // For scroll run
      $('#locationModal').hide();
     }

    });
    });

   // $( "#autocomplete" ).val('Udupi, Karnataka, India');

    
</script>
<style type="text/css">
    #style-switcher {
        display: none;
    }
</style>
      </body>
    </html>
