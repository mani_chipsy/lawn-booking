<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('get_image/{category}/{year}/{month}/{name}.{ext}/{dimention}', 'ImageController@index');

Route::group(['namespace' => 'Web\Admin','prefix' => 'superadmin'], function () {
	
    require(__DIR__ . '/Routes/Web/web_backend_controls.php');
});
Route::group(['namespace' => 'Web\Vendor','prefix' => 'vendor'], function () {
	
    require(__DIR__ . '/Routes/Web/web_vendor_controls.php');
});

Route::group(['namespace' => 'Web\Website'], function () {
    require(__DIR__ . '/Routes/Web/web_website_controls.php');
});
?>