<?php





Route::group(['prefix' => 'user' ], function () {
    Route::post('/login', 'AuthController@login_func');
    Route::post('/register', 'AuthController@register_func');
    Route::get('/refresh', 'AuthController@refresh');
    Route::get('/version/{id}', 'AuthController@vesion_control');
});

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::group(['prefix' => 'posts' ], function () {
        Route::get('/{from}/{type?}/{id?}', 'PostController@index');
    });

    Route::group(['prefix' => 'post' ], function () {
        Route::get('/{postid}/comments', 'PostController@getPostComments');
        Route::get('/{postid}/likDislike', 'PostController@likePost');
        Route::post('/{postid}/add-comment/{c_id?}', 'PostController@addPostComment');
    });

    Route::group(['prefix' => 'mind-pull','namespace' => 'MindPull'], function () {
        Route::get('/questions', 'Questions@index');
        Route::post('/question/{questionId}/new-option', 'Questions@new_option');
        Route::post('/question/{questionId}/answer', 'Questions@answer');
        Route::get('/stats', 'Questions@stats');
    });

    Route::group(['prefix' => 'conversation','namespace' => 'Conversation'], function () {
        Route::get('/videos', 'ConversationVideos@conv_videos_func');
        Route::get('/questiontypes', 'QuestionTypes@questiontype_func');
        Route::get('/present', 'PresentConv@present_conv_func');
        Route::get('/one', 'ConversationOne@conv_one_func');
        Route::post('/one-answer/{questionId}', 'ConversationOneAnswer@conv_one_answer_func');
        Route::get('/two', 'ConversationTwo@conv_two_func');
        Route::post('/two-answer/{questionId}', 'ConversationTwoAnswer@conv_two_answer_func');
        Route::get('/three', 'ConversationThree@conv_three_func');
        Route::post('/three-answer/{questionId}', 'ConversationThreeAnswer@conv_three_answer_func');
        Route::get('/four', 'ConversationFour@conv_four_func');
        Route::post('/four-answer/{questionId}', 'ConversationFourAnswer@conv_four_answer_func');
        Route::get('/five', 'ConversationFive@conv_five_func');
        Route::post('/five-answer/{questionId}', 'ConversationFiveAnswer@conv_five_answer_func');
          
        Route::group(['prefix' => 'result'], function () {
            Route::get('/one', 'GetResultOne@get_result_one_func');
            Route::get('/two', 'GetResultTwo@get_result_two_func');
            Route::get('/three', 'GetResultThree@get_result_three_func');
            Route::get('/four', 'GetResultFour@get_result_four_func');
            Route::get('/five', 'GetResultFive@get_result_five_func');
        });
    });
});