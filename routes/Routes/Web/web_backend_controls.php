<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/signout', 'AuthController@signout_func');
Route::group(['middleware' => ['guest:superadmin', 'revalidate']], function () {
    Route::get('/', 'AuthController@index');
     Route::get('/login', function () {
        return redirect('/superadmin/');
    });
    Route::post('/auth-login', 'AuthController@authlogin_func');
});

/*
|--------------------------------------------------------------------------
| Users Control routes
|
*/

Route::group(['middleware' => ['superadmin', 'revalidate']], function () {
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('settings', 'UserController@get_change_pass');
   
      Route::post('sub-settings', 'UserController@change_settings');
   
 Route::get('/booking-details', 'DashboardController@booking_info');
 Route::get('display-booking-detailes', 'DashboardController@booking_details');
    Route::get('get_image/{category}/{dirYear}/{dirMonth}/{image_name}_{image_size}.{image_ext}', 'ImageController@index');

  

    Route::group(['prefix' => 'change-pass'], function () {
        Route::get('/', 'UserController@get_change_pass');
        Route::post('/', 'UserController@change_pass');
    });

        Route::group(['prefix' => 'vendor'], function () {
                 Route::get('/', 'VendorControl@index');
                 Route::post('/add', 'VendorControl@add');
                 Route::post('/update/{id}', 'VendorControl@update');
                 Route::get('/getdata', 'VendorControl@user_details');
                 Route::get('/edit/{id}', 'VendorControl@edit');
                 Route::post('delete', 'VendorControl@delete');


             });
               Route::group(['prefix' => 'lawn'], function () {
                 Route::get('/', 'GroundControl@index');
                   Route::get('/add', 'GroundControl@get_add_details');
                 Route::post('/add', 'GroundControl@add');
                 Route::post('/update/{id}', 'GroundControl@update');
                 Route::get('/display', 'GroundControl@get_details');
                 Route::get('/edit/{id}', 'GroundControl@edit');
                 Route::delete('delete/{id}', 'GroundControl@delete');


             });

});



/*
|--------------------------------------------------------------------------
| Post Controls
|
*/
