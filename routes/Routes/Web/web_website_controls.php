<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');


 Route::get('get_image/{category}/{year}/{month}/{name}.{ext}/{dimention}', 'ImageController@index');



Route::post('reset_post', 'Auth_Controllers@reset_post');
Route::group(['middleware' => ['guest']], function () {
	
	 Route::get('/pay-now/{amount}', 'HomeController@pay_now');
	 Route::get('/pay-success', 'HomeController@pay_success');
	 Route::get('/lawn/{id}', 'HomeController@get_lawn');
    Route::post('/submit-signup', 'AuthController@submit_signup');
    Route::get('/resend-otp', 'AuthController@resend_otp');
    Route::post('/verify-otp', 'AuthController@verify_otp');

    Route::post('login_post', 'AuthController@post_login');
    Route::post('forgot_post', 'Auth_Controllers@post_forgot');
});

Route::get('/signout', 'AuthController@signout');
Route::post('find-location','HomeController@find_location');

Route::group(['middleware' => ['auth']], function () {

    Route::post('/submit-booking', 'HomeController@post_booking');
});
