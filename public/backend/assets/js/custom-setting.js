$(function($) {


   $('.loaders button').on('click', function () {       
            $data = { 
                autoCheck: $('#autoCheck').is(':checked') ? 32 : false,
                size: $('#size').val(), 
                bgColor: $('#bgColor').val(),  
                bgOpacity: $('#bgOpacity').val(),    
                fontColor: $('#fontColor').val(), 
                title: $('#title').val(), 
                isOnly: !$('#isOnly').is(':checked')
            };
			
            switch ($(this).data('target')){
                case 'body':
                    $.loader.open($data);
                    break;
                case 'self':
                    $(this).loader($data);
                    break;
                case 'form':
                    $('form').loader($data);
                    break;
                case 'close':
                    $.loader.close(true);

            }
        });
    
	
    
});