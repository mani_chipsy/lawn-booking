$(document).ready(function() {

    $('#calendar').fullCalendar({


     defaultDate: Date(),
      editable: true,
      selectable: true,
      selectHelper: true,
      select: function(start, end) {
            var ground_id= $('[name=ground_id]').val();
         var eventData;
         var d = new Date(start);
         var str = $.datepicker.formatDate('yy-mm-dd', d);

         var pr=true;
           $.get('/vendor/lawn/display-booking-detailes/'+ground_id+'?date=' +str , function(response) {
                // $.loader.stop();
                var display = "";
             
                if(response.success){
                  pr=false;
                  display+='<div class="row"> <input type="text" name="booking_id" id="booking_id" value="'+response.data.id+'" hidden="true"><div class="col-lg-6"><div class="example-col">Booking User</div></div>'+
                            '<div class="col-lg-6"><div class="example-col">'+response.data.username+'</div></div></div>'+
                            '<div class="row"><div class="col-lg-6"><div class="example-col">Amount</div></div>'+
                            '<div class="col-lg-6"><div class="example-col">Rs.'+response.data.price+'</div></div></div>';
                            $('#addbook').html(display);
                  $('#addNewEvent1').modal('show');
                  
                }
                else{

                  $('#addNewEvent').modal('show');
                  $('#date').val(str);
                }

         
                
            }, 'json');
           
           
        //$('#calendar').fullCalendar('unselect');
      },
      navLinks: true, // can click day/week names to navigate views
      eventLimit: true, // allow "more" link when too many events
      events: {
        url: '/vendor/lawn/booking_details/'+$('#ground_id').val(),
        error: function() {
          $('#script-warning').show();
        }
      },
      loading: function(bool) {
        $('#loading').toggle(bool);
      }
    });


$('#submit').click(function(){
   var ground_id= $('[name=ground_id]').val();
  swal({   
                title: "Are you sure?",   
                text: "You will  be able to Book This Lawn ",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, Book it!",   
                closeOnConfirm: false 
            }, function(){
               var postdata = new FormData();
               postdata.append('date',$('#date').val());
               postdata.append('desc',$('#ename').val());
                //postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();


                $.ajax({
                    url: "/vendor/lawn/add/"+ground_id,
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.success) {
                            
                          
                            var display = response.message;
                            swal("Success!", 'Successfully updated', "success");
                             $('#addNewEvent').modal('hide');
                         

                    location.reload();
                    
                        } 
                    }
                });
                });
});


$('#submitcancel').click(function(){
  swal({   
                title: "Are you sure?",   
                text: "You will  be able to Book This Lawn ",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, Book it!",   
                closeOnConfirm: false 
            }, function(){
               var postdata = new FormData();
               postdata.append('id',$('#booking_id').val());


                // $.loader.start();


                $.ajax({
                    url: "/vendor/lawn/booking-cancel",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.success) {
                            
                          
                            var display = response.message;
                            swal("Success!", 'Successfully updated', "success");
                             $('#addNewEvent').modal('hide');
                         

                    location.reload();
                    
                        } 
                    }
                });
                });
});

  });