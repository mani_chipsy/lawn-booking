
var myApp = angular.module('lawn', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});
// var myApp = angular.module('lawn', ['ngRoute']); 
myApp.controller("userController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    $scope.showSave = false;
    $scope.showBtns = true;


    //     $scope.reset = function() {
    // $('form[name=user_profile]').get(0).reset();

    //     }

    $scope.profile = function() {
   
        var form = $('[name=user_profile]');
      var proceed =$('[name=user_profile]').parsley().validate();
            // $('[name=user_profile] [required=true]').filter(function() {
            //     if ($.trim(this.value) == "") {
            //         $(this).closest('.form-line').css('border-color', 'red');
            //         proceed = false;
            //     } 
            //   });
        if (proceed) {

            var postdata = {};
            var data = $('[name=user_profile]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });

 $("div#divLoading").addClass('show');
            $.post('/superadmin/vendor/add', postdata, function(response) {
                if (response.success) {
                $("div#divLoading").removeClass('show');
                swal("Success!", 'Vendor created successfully', "success");
                $('form[name=user_profile]').get(0).reset();
                form.find('.resp-msg').html('  ');
                $scope.getUsers(1);
                } 
                else {
                    $("div#divLoading").removeClass('show');
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                     swal("Success!", display, "error");
                    // form.find('.resp-msg').html('* ' + display).css('color', 'red');
                }
            }, 'json');

        }
    }




    $scope.update = function(event) {


        var id = angular.element(event.currentTarget).attr('id');


        // alert(id);
        var form = $('[name=user_profile]');
        var proceed = $('[name=user_profile]').parsley().validate();

        if (proceed) {



           swal({   
                title: "Are you sure?",   
                text: "You will  be able to Update details",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, Update it!",   
                closeOnConfirm: false 
            }, function(){
         


                            var postdata = {};
                            var data = $('[name=user_profile]').serializeArray();
                            $.each(data, function() {
                                postdata[this.name] = this.value || '';
                            });

                         $("div#divLoading").addClass('show');

                            $.post('/superadmin/vendor/update/' + id, postdata, function(response) {
                                if (response.success) {
                                    $("div#divLoading").removeClass('show');
                                     swal("Success!", 'Vendor updated Successfully', "success");
                                     $('form[name=user_profile]').get(0).reset();
                                     $scope.showSave = false;
                                     $('.updatewarden').attr('id','');
                                     $("#editdisable").removeClass("disabledbutton");
                                     $scope.getUsers($scope.currentPage);
                                              
                                } else {
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                     swal("Success!", display, "error");
                                    //form.find('.resp-msg').html('* ' + display).css('color', 'red');
                                }
                            }, 'json');
                       });

        }
    }


    $('input:reset').click(function(){
    $("#editdisable").removeClass("disabledbutton");
    });
    $scope.delete = function(event) {
  
        var id = event.target.id;
  
           swal({   
                title: "Are you sure?",   
                text: "You will  be able to delete details",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, Delete it!",   
                closeOnConfirm: false 
            }, function(){


                          $("div#divLoading").addClass('show');
                        $http.post('/superadmin/vendor/delete', {
                            'id': id
                        }).then(function(response) {
                          $("div#divLoading").removeClass('show');



                           swal("Success!", 'Vendor deleted Successfully"', "success");
                           $('#'+id).closest('tr').hide();
                       

                        });

                         });


                 
           


    }


    $scope.edit = function(event) {


        var id = angular.element(event.currentTarget).attr('id');

        $http.get('/superadmin/vendor/edit/' + id)
            .then(function(res) {
                var id = res.data.id;


                $('#email').val(res.data.email);
                $('#confirm_password').val(res.data.password_str);
                $('#new_password').val(res.data.password_str);



                 $('#phone').val(res.data.mobile_num);
                $('#username').val(res.data.user_name);


                // var change_pass = '<button type="button" id='+res.data._id +' name="update"  class="btn btn-primary waves-effect update">Update</button>';


                //         $('[name="update"]').empty().html(change_pass);
                 $("#editdisable").addClass("disabledbutton");
                $scope.showSave = true;

                $('.updatewarden').attr('id', id);


            });
    }



    // Pagination of Users
    $scope.maxSize = 3;
    $scope.$watch("currentPage", function() {

        $scope.getUsers($scope.currentPage);
    });
       $("div#divLoading").addClass('show');
    //load Users     
    $scope.getUsers = function(page = 1) {
        $http.get('/superadmin/vendor/getdata?page=' + page)
            .then(function(res) {
               $("div#divLoading").removeClass('show');
                $scope.users = res.data.data;

                $scope.totalUsers = res.data.total;
                $scope.currentPage = res.data.current_page;
                $scope.usersPerPage = res.data.per_page;
                $scope.usersFrom = res.data.from ? res.data.from : 0;
                $scope.usersTo = res.data.to ? res.data.to : 0;
            });
    }
}]);