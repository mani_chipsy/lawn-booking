var myApp = angular.module('lawn', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("SettingsController", ['$scope', '$http', function($scope, $http) {
   
$( document ).ready(function() {

    $(document).on('change', '[name="change_pass"]', function() {
        if ($(this).is(':checked')) {
            var change_pass = '<label for="new_password">New Password</label>'
                            +'<div class="form-group">'
                            +'<div class="form-line">'
                            +'<input type="password" required="required" name="new_password" id="new_password" class="form-control" placeholder="New Password" data-parsley-errors-container="#new_pass_error">'
                            +'</div>'
                            +'<div id="new_pass_error"></div>'
                            +'</div>'
                            +'<label for="confirm_password">Confirm Password</label>'
                            +'<div class="form-group">'
                            +'<div class="form-line">'
                            +'<input type="password" required="required" data-parsley-equalto="#new_password" data-parsley-equalto-message="Confirm Password should match the New Password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Repeat Password" data-parsley-errors-container="#rep_pass_error">'
                            +'</div>'
                            +'<div id="rep_pass_error"></div>'
                            +'</div>';

            $('[name="checkd-chg-pass"]').empty().html(change_pass);
        } else {
            $('[name="checkd-chg-pass"]').empty();
        }
    });
 $(document).on('click', '[name=updateProfile]', function() {
    // alert("hii");
  
        var form = $('[name=update_profile]');
        var proceed = $('[name=update_profile]').parsley().validate();

        if (proceed) {
             swal({   
                title: "Are you sure?",   
                text: "You will Change your Settings",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, change it!",   
                closeOnConfirm: false 
            }, function(){
                            // var postdata = {};
                            // var data = $('[name=update_profile]').serializeArray();
                            // $.each(data, function() {
                            //     postdata[this.name] = this.value || '';
                            // });
                // var postdata = new FormData();
                // postdata.append('lco_num', $('#lco_num').val());
                // postdata.append('name', $('#lco_name').val());
                // postdata.append('gst_num', $('#gst_no').val());
                // postdata.append('group_id', $('#group_id').val());
                // postdata.append('email', $('#email').val());
                // postdata.append('mobile', $('#mobile').val());
                // postdata.append('district', $('#district').val());
                // postdata.append('address', $('#address').val());
                // postdata.append('city', $('#city').val());
                // postdata.append('pin_code', $('#pin_code').val());

                // postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));


                var postdata = new FormData($('[name=update_profile]')[0]);

                   $.ajax({
                    url: '/superadmin/sub-settings',
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data:postdata,
                    success: function(response) {
                           
                                if (response.success) {
                                 var display = response.message;
                                  $('[name="lco-display"]').show();
                                    $('#add-lco').hide();
                                    $('#current_password').val('');
                                    $('[name="checkd-chg-pass"]').empty();
                                  swal("Success!", 'Successfully updated', "success");

                                } else {
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                    // form.find('.resp-msg').html('* '+display).css('color', 'red');
                                    swal(display);
                                }
                            }
                        });
                        
                   });
                }
            
    });

});

}]);