$.book = {
    init: function(callback) {
        $(document).on('click', '[name="booking_lawn"]', function() {
           
            $.book.form_validate($(this), callback);
        });

        $(document).on('click', '#verify_otp', function() {
            $.book.otpVerify();
        });

        $(document).on('click', '#resend_otp', function() {
            $.book.resendOtp();
        });
    },
    form_validate: function(submitbutton) {
        var form = submitbutton.parents('form');
        var proceed = form.parsley().validate();
        if (proceed) $.book.form_submit(form);
    },

    form_submit: function(form) {
        uploadObject = form.ajaxSubmit({
            beforeSubmit: function() {
                // $.loader.start();
            },
            uploadProgress: function(event, position, total, percentComplete) {

            },
            success: function(response) {
                // $.loader.stop();
                if (response.success) {
                 
                } 
                else {
                console.log(response);
                var display = "";
                if ((typeof response.message) == 'object') {
                $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    // console.log(display);
                    form.find('.signup-error').html('* '+display).css('color', 'red');
                    // $('[name=user-register] input').val('');
                }
            },
            error: function() {
                alert('error');
                // $.loader.stop();
                // $.confirm({
                //     title: 'Error!',
                //     content: response.message,
                //     buttons: {
                //         OK: function() {

                //         },

                //     }
                // });
            },
        });
    },
    resendOtp : function() {
        $.get('/resend-otp', function(response) {
            if (response.success) {
                $('#resp-msg').removeClass('text-danger').addClass('text-success').text(response.message);
            }
        })
    },
    otpVerify : function() {
        var proceed = $('[name=otp_verify]').parsley().validate();
        if (proceed) {
            var postdata = {};
            var data = $('[name=otp_verify]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            $.post('/verify-otp', postdata, function(response) {
                if (response.success) {
                    location.href =  response.redirect;
                    $('#quick-shop-modal').find('.modal-dialog').removeClass('otp-modal');
                    $('#quick-shop-modal .modal-body').empty();
                    $('#quick-shop-modal').modal('hide');
                } else {
                    $('#resp-msg').removeClass('text-success').addClass('text-danger').text(response.message);
                }
            })
        } else {
            $('.clear-fix').html('');
        }
    }
}
$.book.init();